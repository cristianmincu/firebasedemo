//
//  DetailUserViewController.swift
//  TestUsers
//
//  Created by Mincu, Cristian on 2019-09-02.
//  Copyright © 2019 Mincu, Cristian. All rights reserved.
//

import Foundation
import Firebase
import FirebaseStorage
import FirebaseUI

class DetailUserViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate, UITableViewDataSource {
    
    var user: User?
    var accountEmail: String?
    
    var avatar: UIImage? = nil
    var hobbies = [String]()
    
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var middleName: UITextField!
    @IBOutlet weak var born: UITextField!
    
    @IBOutlet weak var hobbyTestfield: UITextField!
    
    @IBOutlet weak var addHobbyBtn: UIButton!
    @IBOutlet weak var hobbyTxt: UITextField!
    @IBOutlet weak var hobbyTable: UITableView!
    
    @IBOutlet weak var saveBtn: UIButton!
    
    let ref = Database.database().reference(withPath: "users")
    
override func viewDidLoad() {
    super.viewDidLoad()
    
    if let user = self.user {
        ref.child(user.key).observeSingleEvent(of: .value, with: { (snapshot) in
                
                    let storage = Storage.storage()
                    let imageRef = storage.reference(withPath: user.avatar!)
                
                    imageRef.getData(maxSize: 15 * 1024 * 1024) { (data, error) in
                        if error == nil && data != nil {
                            //let image = UIImage(data: data!)
                            
                            self.avatarImage.sd_setImage(with: imageRef, placeholderImage: UIImage(named: "placeholder.png"), completion: { (image, error, cacheType, storageRef) in
                                    print(error.debugDescription)
                            })
                        } else {
                            print(error.debugDescription)
                        }
                    }
            
        })
        
        firstName.text = user.firstName
        lastName.text = user.lastName
        middleName.text = user.middleName
        born.text = String(user.born)
        
        hobbies = user.hobbies ?? []
        
    }
    
}

    
    
    // MARK: UITableView Delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return hobbies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "hobby", for: indexPath)
        
        cell.textLabel?.text = hobbies[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            hobbies.remove(at: indexPath.row)
            hobbyTable.reloadData()
        }
    }
    
    
@IBAction func addHobbyAC(_ sender: Any) {
    
    guard !self.hobbyTxt.text!.isEmpty
        else { return }
    
    if !hobbies.contains(self.hobbyTxt.text!) {
        
        hobbies.append(self.hobbyTxt.text!)
        hobbyTable.reloadData()
        
    }
    
}
    
    
@IBAction func saveUserAC(_ sender: Any) {
    
    saveUserData()
        
}
    
@IBAction func pickImage(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose picker", message: "Please select an Option", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Image from Gallery", style: .default, handler: { (_) in
            
            let pickerController = UIImagePickerController()
            pickerController.delegate = self;
            pickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
            
            self.present(pickerController, animated: true, completion: nil)
 
        }))
        
        alert.addAction(UIAlertAction(title: "Image from Camera", style: .default, handler: { (_) in
            
            let pickerController = UIImagePickerController()
            pickerController.delegate = self;
            pickerController.sourceType = UIImagePickerController.SourceType.camera
            
            self.present(pickerController, animated: true, completion: nil)

        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { (_) in

        }))
        self.present(alert, animated: true, completion: nil)

}
    
func saveUserData() {
    
    guard !self.firstName.text!.isEmpty,
          !self.lastName.text!.isEmpty,
          !self.born.text!.isEmpty
    else { return }
    
    var userItem: User
    
    let timestamp = Int(NSDate.timeIntervalSinceReferenceDate*1000)
    
    if user == nil {
       userItem = User(addedByUser: self.accountEmail ?? "",
                        
                        firstName: self.firstName.text!,
                        lastName: self.lastName.text!,
                        middleName: self.middleName.text ?? "",
                        born: self.born.text!.toInt()!,
                        hobbies: hobbies.count != 0 ? hobbies : [""],
                        avatar: self.avatar != nil ? "\("userAvatar")/\(String(timestamp))" : "")
        
        
        let userRef = self.ref.child(String(timestamp))
        
        userRef.setValue(userItem.toAnyObject())
    }
    else
    {
        userItem = self.user!
        
        let userRef = self.ref.child(user!.key)
        
        userRef.updateChildValues(["firstName": self.firstName.text!,
                                   "lastName": self.lastName.text!,
                                   "middleName": self.lastName.text ?? "",
                                   "born": self.born.text!.toInt()!,
                                   "hobbies": self.hobbies,
                                   "avatar": self.avatar != nil ? "\("userAvatar")/\(String(timestamp))" : ""])
        
    }
    
    if self.avatar != nil {
        
        let storage = Storage.storage()
        var data = Data()
        data = (self.avatarImage.image?.pngData())!;
        let storageRef = storage.reference()
        print(storageRef)
        
        let imageRef = storageRef.child(userItem.avatar!)
        
        imageRef.putData(data,metadata: nil,completion: { (metadata,error) in
            guard metadata != nil else {
                print(error.debugDescription)
                return
            }
        })
        
    }

    
    performSegue(withIdentifier: "back", sender: self)
}
    
    func imagePickerController(_ picker: UIImagePickerController,didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.avatar = info[.originalImage] as? UIImage;
        avatarImage.image = avatar
        
        self.dismiss(animated: true, completion: nil)
        
    }
    

}
