//
//  ViewController.swift
//  TestUsers
//
//  Created by Mincu, Cristian on 2019-09-01.
//  Copyright © 2019 Mincu, Cristian. All rights reserved.
//

import UIKit
import Firebase

class UserTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userImage: UIImageView!
    
    @IBOutlet weak var firstName: UILabel!
    @IBOutlet weak var lastName: UILabel!
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: Constants
    let userDetails = "userDetail"
    
     var todoData = [String]()
    
    // MARK: Properties
    var users: [User] = []
    var account: Account?
    var userParam: User? = nil
    let ref = Database.database().reference(withPath: "users")
    let usersRef = Database.database().reference(withPath: "demo")
    
    @IBOutlet weak var maintableview: UITableView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.maintableview.allowsMultipleSelectionDuringEditing = false
        
        Auth.auth().addStateDidChangeListener { auth, user in
            guard let user = user else { return }
            self.account = Account(authData: user)
            
            let currentUserRef = self.usersRef.child(self.account!.uid)
            currentUserRef.setValue(self.account?.email)
            currentUserRef.onDisconnectRemoveValue()
        }
    
        populate()
        
    }
    
    // MARK: UITableView Delegate methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! UserTableViewCell
        
        let userItem = users[indexPath.row]
        
        cell.firstName.text = userItem.firstName
        cell.lastName.text = userItem.lastName
        
        let storage = Storage.storage()
        let storageRef = storage.reference(withPath: userItem.avatar!)
        
        storageRef.downloadURL { url, error in
            if let error = error {
                print("error: \(error.localizedDescription)")
            } else {
                if let d_url = url?.absoluteString {
                    cell.userImage.getRemoteImage(url: d_url, placeholder: "placeholder")
                }
            }
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let userItem = users[indexPath.row]
            userItem.ref?.removeValue()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        userParam = users[indexPath.row]
        
        self.performSegue(withIdentifier: self.userDetails, sender: self)
 
    }
    

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == self.userDetails, segue.destination is UINavigationController {
            
               let navVC = segue.destination as? UINavigationController
            
               let targetController = navVC?.viewControllers.first as! DetailUserViewController
            
                //let targetController = segue.destination as! DetailUserViewController
                targetController.user = self.userParam
                targetController.accountEmail = self.account?.email
            
        }
        
    }
    
    @IBAction func backToMain(_ unwindSegue: UIStoryboardSegue) {
        
        populate()
        
    }
    
    func populate() {
      
        
        ref.queryOrdered(byChild: "firstName").observe(.value, with: { snapshot in
            var newusers: [User] = []
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot,
                    let userItem = User(snapshot: snapshot) {
                    newusers.append(userItem)
                }
            }
            
            self.users = newusers
            self.maintableview.reloadData()
        })
    
        
    }
    
    @IBAction func addButtonDidTouch(_ sender: AnyObject) {
        
         self.performSegue(withIdentifier: self.userDetails, sender: self)

    }
    
    @IBAction func logout(_ sender: Any) {
        do {
            try Auth.auth().signOut()
            performSegue(withIdentifier: "back", sender: self)
        } catch {
            print("No user is logged in")
        }
    }
    
}
