//
//  String.swift
//  TestUsers
//
//  Created by Mincu, Cristian on 2019-09-02.
//  Copyright © 2019 Mincu, Cristian. All rights reserved.
//

import Foundation

extension String {
    //Converts String to Int
   public func toInt() -> Int? {
      if let num = NumberFormatter().number(from: self) {
        return num.intValue
      } else {
        return nil
      }
   }
}
