//
//  UIImageView.swift
//  TestUsers
//
//  Created by Mincu, Cristian on 2019-09-02.
//  Copyright © 2019 Mincu, Cristian. All rights reserved.
//


import UIKit
import AlamofireImage

extension UIImageView{
    
    public func getRemoteImage(url: String, placeholder: String){
        
        self.af_setImage(withURL: URL(string: url)!, placeholderImage: UIImage(named: placeholder), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .crossDissolve(0.5), runImageTransitionIfCached: true, completion: { response in
            if response.response != nil {
                print("Image loaded")
            }
            else
            {
                print("Failed to download")
            }
        })
        
    }
}
