//
//  Account.swift
//  TestUsers
//
//  Created by Mincu, Cristian on 2019-09-01.
//  Copyright © 2019 Mincu, Cristian. All rights reserved.
//

import Foundation
import Firebase

struct Account {
    
    let uid: String
    let email: String
    
    init(authData: Firebase.User) {
        uid = authData.uid
        email = authData.email!
    }
    
    init(uid: String, email: String) {
        self.uid = uid
        self.email = email
    }
}
