//
//  User.swift
//  TestUsers
//
//  Created by Mincu, Cristian on 2019-09-01.
//  Copyright © 2019 Mincu, Cristian. All rights reserved.
//

import Foundation
import Firebase

struct User {
    
    let ref: DatabaseReference?
    
    let firstName: String
    let lastName: String
    let middleName: String?
    let born: Int
    let hobbies: [String]?
    let avatar: String?
    
    let key: String
    let addedByUser: String
    
    init(addedByUser: String,
         key: String = "",
         
         firstName: String,
         lastName: String,
         middleName: String?,
         born: Int,
         hobbies: [String]?,
         avatar: String?) {
        
        self.ref = nil
        
        self.firstName = firstName
        self.lastName = lastName
        self.middleName = middleName
        self.born = born
        self.hobbies = hobbies
        self.avatar = avatar
        
        self.key = key
        self.addedByUser = addedByUser
    }
    
    init?(snapshot: DataSnapshot) {
        guard
            let value = snapshot.value as? [String: AnyObject],
            let addedByUser = value["addedByUser"] as? String,
            
            let firstName = value["firstName"] as? String,
            let lastName = value["lastName"] as? String,
            let middleName = value["middleName"] as? String,
            
            let born = value["born"] as? Int,
            let hobbies = value["hobbies"] as? [String],
            let avatar = value["avatar"] as? String else {
                return nil
        }
        
        self.ref = snapshot.ref
        self.key = snapshot.key
        
        self.firstName = firstName
        self.lastName = lastName
        self.middleName = middleName
        self.born = born
        self.hobbies = hobbies
        self.avatar = avatar
        
        self.addedByUser = addedByUser
    }
    
    func toAnyObject() -> Any {
        return [
            "firstName" : firstName,
            "lastName" : lastName,
            "middleName" : middleName ?? "",
            "born" : born,
            "hobbies" : hobbies ?? [],
            "avatar" : avatar ?? "",
            
            "addedByUser": addedByUser
        ]
    }
}
